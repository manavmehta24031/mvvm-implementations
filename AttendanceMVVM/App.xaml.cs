﻿using AttendanceMVVM.Services;
using AttendanceMVVM.ViewModel;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace AttendanceMVVM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly ServiceProvider _serviceProvider;
        public App()
        {
            IServiceCollection services = new ServiceCollection();

            //registering using a factory function 
            //the reason we want it as a factory function because we need to set the DataContext  
            services.AddSingleton<MainWindow>(provider => new MainWindow
            {
                DataContext = provider.GetRequiredService<MainViewModel>()
            });
            services.AddSingleton<MainViewModel>();
            services.AddSingleton<EmployeeLoginViewModel>();
            services.AddSingleton<AdminLoginViewModel>();
            services.AddSingleton<INavigationService, NavigationServices>(); 

            services.AddSingleton<Func<Type, Core.ViewModel>>(provider => viewModelType =>(Core.ViewModel)provider.GetRequiredService(viewModelType));

            _serviceProvider = services.BuildServiceProvider();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var mainwindow = _serviceProvider.GetRequiredService<MainWindow>();
            mainwindow.Show(); 
            base.OnStartup(e);
        }
    }
}
