﻿using MVVMViews.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVMViews.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {


        private BaseViewModel _selectedViewModel;
        public BaseViewModel SelectedViewModel
        {
            get
            {
                return _selectedViewModel;
            }
            set
            {
                _selectedViewModel = value;
                OnPropertyChanged(nameof(SelectedViewModel));
            }
        }

        private ICommand _navigateView;
        public ICommand NavigateView
        {
            get
            {
                if (_navigateView == null)
                {
                    _navigateView = new RelayCommand(ChangeView, CanChangeView);
                }
                return _navigateView;
            }
            set
            {
                _navigateView = value;                
            }
        }

        private bool _btnVisible;
        public bool btnVisible
        {
            get { return _btnVisible; }
            set
            {
                _btnVisible = value;
                OnPropertyChanged(nameof(btnVisible));
            }
        }

        private bool _isAdminLoggedIn;
        public bool IsAdminLoggedIn
        {
            get { return _isAdminLoggedIn; }
            set
            {
                _isAdminLoggedIn = value;
                OnPropertyChanged(nameof(IsAdminLoggedIn));
            }
        }


        private bool _isEmployeeLoggedIn;
        public bool IsEmployeeLoggedIn
        {
            get { return _isEmployeeLoggedIn; }
            set
            {
                _isEmployeeLoggedIn = value;
                OnPropertyChanged(nameof(IsEmployeeLoggedIn));
            }
        }

        private bool CanChangeView(object parameter)
        {
            return true;
        }

        private void ChangeView(object parameter)
        {
            if (parameter.ToString() == "Admin")
            {
                SelectedViewModel = new AdminLoginViewModel();
                btnVisible = true;
            }
            else if (parameter.ToString() == "Employee")
            {
                SelectedViewModel = new EmployeeLoginViewModel();
                btnVisible = true;

            }
            else if(parameter.ToString() == "Home")
            {
                SelectedViewModel = new HomeViewModel();
                btnVisible = false;
            }
        }
    }
}
