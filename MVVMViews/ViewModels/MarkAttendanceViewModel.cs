﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMViews.ViewModels
{
    public class MarkAttendanceViewModel : BaseViewModel
    {
        public string Date = DateTime.Now.ToString("d");
        public string Month = DateTime.Now.ToString("MMMM");
        public string Year = DateTime.Now.ToString("yyyy");

        //public string Date
        //{
        //    get { return _date; }
        //    set { 
        //        _date = DateTime.Now.ToString("d"); 
        //        OnPropertyChanged(nameof(Date));
        //    }
        //}

        //public string Month
        //{
        //    get { return _month; }
        //    set { 
        //        _month = DateTime.Now.ToString("MMMM"); 
        //        OnPropertyChanged(nameof(Month));
        //    }
        //}

        //public string Year
        //{
        //    get { return _year; }
        //    set { 
        //        _year = DateTime.Now.ToString("yyyy"); 
        //        OnPropertyChanged(nameof(Year));   
        //    }
        //}
    }
}
