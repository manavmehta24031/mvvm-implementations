﻿using MVVMViews.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MVVMViews.ViewModels;
using MVVMViews.Views;
using System.Windows;

namespace MVVMViews.ViewModels
{
    public class AdminLoginViewModel : BaseViewModel
    {

        private BaseViewModel _selectedViewModel;
        public BaseViewModel SelectedViewModel
        {
            get
            {
                return _selectedViewModel;
            }
            set
            {
                _selectedViewModel = value; 
                OnPropertyChanged(nameof(SelectedViewModel));
            }
        }

        private ICommand _navigateView;
        public ICommand NavigateView
        {
            get
            {
                if (_navigateView == null)
                {
                    _navigateView = new RelayCommand(ChangeView, CanChangeView);
                }
                return _navigateView;
            }
            set
            {
                _navigateView = value;
            }
        }

        private bool _visibility;
        public bool Visibility
        {
            get { return _visibility; }
            set
            {
                _visibility = value;
                OnPropertyChanged(nameof(Visibility));
            }
        }

        private bool CanChangeView(object parameter)
        {
            return true;
        }


        private void ChangeView(object parameter)
        {
            if (parameter.ToString() == "Enroll")
            {
                SelectedViewModel = new EnrollEmployeeViewModel();
                Visibility = true;
            }
            else if (parameter.ToString() == "Department")
            {
                SelectedViewModel = new AddDepartmentViewModel();
                Visibility = true;
            }


        }
    
        
        
    
    }
}

